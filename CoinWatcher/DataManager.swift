//
//  DataManager.swift
//  CoinWatcher
//
//  Created by Stuart Levine on 3/30/18.
//  Copyright © 2018 Brave. All rights reserved.
//
//  This manager abstracts away the direct implementation of core data from the main app.
//  And, this is a good design pattern in the case where one may decide to use an alternative
//  framework for local/remote cachine (Realm for instance)

import Foundation
import CoreData

//  Set up dependency injection using protocol extensions

public protocol DataManagerInjected { }

public extension DataManagerInjected {
    public var dataManager: DataManager { get { return DataManager.shared } }
}

// DataManagerProtocol
//
// I always like to define a protocol for open classes to provide
// the minimum required methods when overriding the class
//
// Note: If I had more time I would have implemted this to be more thread
//       safe by using a serial operation queue and operations

public protocol DataManagerProtocol {
    func saveContext()
    func coin(id: String) -> Coin?
    func insertNewFavoriteCoin(id: String) -> Coin
    func updateCoin(id: String, isFavorite: Bool)
}

// Main DataManager
open class DataManager: DataManagerProtocol {
    // The singleton
    public static let shared = DataManager()

    private let persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CoinWatcher")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // Utility function take from the original AppDelegate

    public func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    public func coin(id: String) -> Coin? {
        let request: NSFetchRequest<Coin> = Coin.fetchRequest()
        request.predicate = NSPredicate(format: "coinId = %@", id)
        let result = try? persistentContainer.viewContext.fetch(request)
        return result?.first
    }

    // Updates the favorite flag for a Coin. If the record does not exist yet
    // Create it.
    //
    public func updateCoin(id: String, isFavorite: Bool) {
        if let coin = coin(id: id) {
            coin.isFavorited = isFavorite
            saveContext()
        }
        else {
            let coin = insertNewFavoriteCoin(id: id)
            coin.isFavorited = isFavorite
            saveContext()
        }
    }

    public func insertNewFavoriteCoin(id: String) -> Coin {
        let entity = NSEntityDescription.entity(forEntityName: Coin.description(), in: persistentContainer.viewContext)!
        let new = Coin(entity: entity, insertInto: persistentContainer.viewContext)
        new.coinId = id
        new.isFavorited = false
        saveContext()
        return new
    }
}
