//
//  MasterViewController.swift
//  CoinWatcher
//
//  Created by Joel Reis on 3/23/18.
//  Copyright © 2018 Brave. All rights reserved.
//
//  Stuart's Notes:
//  --------------
//
//  - made improvements on the dataSource using URLSession with URLSessionDataTask to provide asynchronous remote data loading.
//    If I had more time I would have broken this out into a NetworkManager class and protocol to abstract away the use of
//    URLSession so that we could replace standart Apple SDK calls with Alamofire or any other equiv network framework if desired.
//
//  - I decided to break the table up into two sections, favorites and coins.  While attempting to used sorted() on a single section
//    I discovered it was impossible to sort the data alphabetically and provide the favorites at the top of the table
//    with consistency.  It also made it easier to provide animated addition and removal of favorites from the tableView.
//

import UIKit

// Define our block parameters
typealias CompletionBlock = ([[String: Any]])->()
typealias CoinDataSource = [Int: [MasterCellViewModel]]

// Some constants
let nameKey = "name"
let currency = "CAD"
let coinCellIdentifier = "coinCellIdentifier"

// TableView Sections
enum Section: Int {
    case favorites
    case coins
}

class MasterViewController: UITableViewController {
    fileprivate var dataSource = CoinDataSource()
    fileprivate var detailViewController: DetailViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Added pull to refresh
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = UIColor.lightGray
        refreshControl?.addTarget(self, action: #selector(reloadData(_:)), for: .valueChanged)

        self.navigationItem.title = "Coin Watcher"

        tableView.register(MasterCell.self, forCellReuseIdentifier: coinCellIdentifier)

        fetchData()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    // Refresh controler action
    @objc func reloadData(_ refreshControl: UIRefreshControl) {
        fetchData()
    }

    // MARK: - Table View

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource[section]?.count ?? 0
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: coinCellIdentifier, for: indexPath)
        let masterCell = cell as? MasterCell
        masterCell?.delegate = self
        let viewModel = dataSource[indexPath.section]?[indexPath.row]
        masterCell?.bind(to: viewModel)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = DetailViewController(with: dataSource, selectedIndexPath: indexPath)
        detailVC.delegate = self
        detailVC.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
        detailVC.navigationItem.leftItemsSupplementBackButton = true
        detailViewController = detailVC
        let controller = UINavigationController(rootViewController: detailVC)
        self.showDetailViewController(controller, sender: self)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MasterCell.cellHeight
    }

    // Main fetchData method - should be broken out into a NetworkManager class
    func fetchData() {
        loadData { [weak self] (results) in
            guard let `self` = self else { return }

            let coinData = results
                .map({ (coinDict) -> MasterCellViewModel? in
                    guard let name = coinDict[nameKey] as? String else { return nil }
                    let priceKey = "price_\(currency.lowercased())"
                    let price = coinDict[priceKey] as? NSString
                    let viewModel = MasterCellViewModel(name: name, price: price?.doubleValue ?? 0.0, currency: currency)
                    return viewModel
                })
                .compactMap {$0}
                .sorted { $0.name < $1.name }

            let favorites = coinData.filter { $0.isFavorite }
            let coins = coinData.filter { !$0.isFavorite }

            self.dataSource[Section.favorites.rawValue] = favorites
            self.dataSource[Section.coins.rawValue] = coins

            DispatchQueue.main.async {
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
                let indexPath = self.tableView.indexPathForSelectedRow
                // On iPad in, splitView mode, make sure to select the first row of the tableView
                if self.traitCollection.userInterfaceIdiom == .pad, indexPath == nil, self.dataSource.count > 0 {
                    if self.dataSource[Section.favorites.rawValue]?.count ?? 0 > 0 {
                        let indexPath = IndexPath(item: 0, section: Section.favorites.rawValue)
                        self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                        self.tableView(self.tableView, didSelectRowAt: indexPath)
                    }
                    else if self.dataSource[Section.coins.rawValue]?.count ?? 0 > 0 {
                        let indexPath = IndexPath(item: 0, section: Section.coins.rawValue)
                        self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                        self.tableView(self.tableView, didSelectRowAt: indexPath)
                    }
                }
            }
        }
    }

    // MARK: - Load the Remote Data Source

    func loadData(onCompletion: CompletionBlock?) {
        guard let url = URL(string: "https://api.coinmarketcap.com/v1/ticker/?limit=25&convert=\(currency)") else { return }

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        let task = session.dataTask(with: url) { (data, response, error) in
            if let data = data, let jsonData = try? JSONSerialization.jsonObject(with: data, options: []), let results = jsonData as? [[String:Any]] {
                onCompletion?(results)
            }
            else if let _ = error {
                // If I had more time I would have implemented either an error block or returned an enum type
                // with associated parameters to provide a result or error type.  then I would leave the error
                // reporting to the VC
            }
        }
        task.resume()
    }

}

// Conform to the DetailViewController's delegate to allow for selection of rows when a user taps the forward and back buttons
extension MasterViewController: DetailViewControllerDelegate {
    func didShow(_ detailItem: MasterCellViewModel, at indexPath: IndexPath) {
        if traitCollection.userInterfaceIdiom == .pad && self.dataSource.count > 0 {
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            tableView(tableView, didSelectRowAt: indexPath)
        }
    }
}

// Conform to the MasterCellDelegate protocol to provide actions for when someone taps the favorite button
extension MasterViewController: MasterCellDelegate {
    func didTapFavoriteButton(_ viewModel: MasterCellViewModel?) {
        let selectedIndexPath = tableView.indexPathForSelectedRow
        var currentSelectedItem: MasterCellViewModel?
        if let selectedIndexPath = selectedIndexPath {
            currentSelectedItem = self.dataSource[selectedIndexPath.section]?[selectedIndexPath.row]
        }

        guard let viewModel = viewModel, var favorites = dataSource[Section.favorites.rawValue], var coins = dataSource[Section.coins.rawValue] else { return }

        if viewModel.isFavorite {
            if let index = coins.index(where: { (model) -> Bool in
                model.name == viewModel.name
            }) {
                dataSource[Section.coins.rawValue]?.remove(at: index)
                tableView.deleteRows(at: [IndexPath(item: index, section: Section.coins.rawValue)], with: .automatic)
                favorites.append(viewModel)
                dataSource[Section.favorites.rawValue] = favorites.sorted { $0.name < $1.name }
                tableView.reloadSections([Section.favorites.rawValue], with: .automatic)
            }
        }
        else {
            if let index = favorites.index(where: { (model) -> Bool in
                model.name == viewModel.name
            }) {
                dataSource[Section.favorites.rawValue]?.remove(at: index)
                tableView.deleteRows(at: [IndexPath(item: index, section: Section.favorites.rawValue)], with: .automatic)
                coins.append(viewModel)
                dataSource[Section.coins.rawValue] = coins.sorted { $0.name < $1.name }
                tableView.reloadSections([Section.coins.rawValue], with: .automatic)
            }
        }
        // if we are on an iPad, we need to make sure to reselect the selected Row if we changed
        // said row's favorite status becuase the data source will be modified and resorted.
        if let currentItem = currentSelectedItem, traitCollection.userInterfaceIdiom == .pad {
            for section in dataSource.keys {
                for (index, item) in (dataSource[section] ?? []).enumerated() {
                    if item.name == currentItem.name {
                        let indexPath = IndexPath(item: index, section: section)
                        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                        break
                    }
                }
            }

        }
        // We need to update the DetailVC datasource since we've modified the tableView data source and sorting.
        // otherwise we would see anomalous behavior when traversing the data source in that view using the forward and back buttons
        self.detailViewController?.update(dataSource: self.dataSource)
    }
}
