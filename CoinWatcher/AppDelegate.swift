//
//  AppDelegate.swift
//  CoinWatcher
//
//  Created by Joel Reis on 3/23/18.
//  Copyright © 2018 Brave. All rights reserved.
//
//  Notes by Stuart:
//
//  Some changes made:
//      - found a few instances of force unwrapped optional use.  Usually not a good idea so I change then to use
//        optional chaining.
//      - Removed core data setup and methods to a DataManager, and use protocol injection to provide this manager to
//        classes by having them conform to the DataManagerInjected protocol
//      - Change var to let for the window property because I did not see the need for mutation

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate, DataManagerInjected {

    let window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let splitVC = UISplitViewController()
        let masterVC = UINavigationController(rootViewController: MasterViewController())
        let detailVC = UINavigationController(rootViewController: DetailViewController())
        splitVC.viewControllers = [masterVC, detailVC]
        splitVC.delegate = self
        
        detailVC.topViewController!.navigationItem.leftBarButtonItem = splitVC.displayModeButtonItem
        
        window?.rootViewController = splitVC
        
        return true
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window?.makeKeyAndVisible()
        return true
    }


    func applicationWillTerminate(_ application: UIApplication) {
        dataManager.saveContext()
    }

    // MARK: - Split view

    func splitViewController(_ svc: UISplitViewController, shouldHide vc: UIViewController, in orientation: UIInterfaceOrientation) -> Bool {
        return false
    }

    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
        guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
        guard let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController else { return false }
        if topAsDetailController.detailItem == nil {
            // Return true to indicate that we have handled the collapse (by doing nothing)
            return true
        }
        return false
    }

}

