//
//  MasterCell.swift
//  CoinWatcher
//
//  Created by Stuart Levine on 4/1/18.
//  Copyright © 2018 Brave. All rights reserved.
//
//  Moved MasterCell, MasterCellViewModel and MasterCellDelegate protocols to a separate file for clarity and structure

import Foundation
import UIKit

// This is viewModel and data source for our MasterCell.

public class MasterCellViewModel: DataManagerInjected {

    let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .currency
        nf.currencySymbol = "$"
        return nf
    }()
    var isFavorite: Bool {
        get {
            return self.coin(id: self.name).isFavorited
        }
        set {
            self.dataManager.updateCoin(id: self.name, isFavorite: newValue)
        }
    }
    var name: String
    var price: Double
    var currency: String
    var priceString: String {
        let number = NSDecimalNumber(value: self.price)
        return self.numberFormatter.string(from: number) ?? "$\(self.price)"
    }
    init(name: String, price: Double = 0.0, currency: String = "USD") {
        self.name = name
        self.price = price
        self.currency = currency
    }

    func coin(id: String) -> Coin {
        if let coin = dataManager.coin(id: name) {
            return coin
        }
        else {
            return dataManager.insertNewFavoriteCoin(id: id)
        }

    }

}

// Define the MasterCellDelegate methods
public protocol MasterCellDelegate : class {
    func didTapFavoriteButton(_ viewModel: MasterCellViewModel?)
}

// I made several improvements and added labels for the coin name and price.  I also used anchors to layout the cell

public class MasterCell: UITableViewCell {

    weak var delegate: MasterCellDelegate?
    static let cellHeight: CGFloat = 60.0

    var viewModel: MasterCellViewModel?

    var favBtn: UIButton = {
        var btn = UIButton()
        btn.setImage(UIImage(named: "heart.png")!, for: .normal)
        btn.heightAnchor.constraint(equalToConstant: 25).isActive = true
        btn.widthAnchor.constraint(equalToConstant: 25).isActive = true
        btn.tintColor = .lightGray
        btn.isSelected = false
        return btn
    }()

    var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: 20.0)
        label.lineBreakMode = .byTruncatingTail
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        return label
    }()

    var priceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: 16.0)
        label.lineBreakMode = .byTruncatingTail
        label.textColor = UIColor.darkGray
        label.font = UIFont.systemFont(ofSize: 14.0)
        return label
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        contentView.addSubview(nameLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(favBtn)

        nameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 15).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -8).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: favBtn.leftAnchor, constant: 10).isActive = true

        priceLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor).isActive = true
        priceLabel.rightAnchor.constraint(equalTo: nameLabel.rightAnchor).isActive = true
        priceLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: 12).isActive = true

        favBtn.translatesAutoresizingMaskIntoConstraints = false
        favBtn.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -15).isActive = true
        favBtn.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        favBtn.addTarget(self, action: #selector(favoriteSelected), for: .touchUpInside)
    }

    // Called to bind the viewModel data to the cell UI components.
    func bind(to viewModel: MasterCellViewModel?) {
        guard let viewModel = viewModel else { return }

        self.viewModel = viewModel
        nameLabel.text = viewModel.name
        priceLabel.text = "\(viewModel.priceString) \(viewModel.currency)"
        favBtn.isSelected = viewModel.isFavorite
        favBtn.tintColor = favBtn.isSelected ? .red : .lightGray
    }

    @objc public func favoriteSelected(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        sender.tintColor = sender.isSelected ? .red : .lightGray
        viewModel?.isFavorite = sender.isSelected
        delegate?.didTapFavoriteButton(viewModel)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // Since cells are dequeuable (and only initialized when needed for cells visible on the screen,
    // it is always good practive to provide this to reset various properties.
    override public func prepareForReuse() {
        super.prepareForReuse()

        favBtn.isSelected = false
        nameLabel.text = ""
        priceLabel.text = ""
    }

}
