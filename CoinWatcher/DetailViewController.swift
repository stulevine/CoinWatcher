//
//  DetailViewController.swift
//  CoinWatcher
//
//  Created by Joel Reis on 3/23/18.
//  Copyright © 2018 Brave. All rights reserved.
//
//  Stuart's Notes:
//  --------------
//  - Provided forward and back UI elements and button implementation
//  - Added price to Name label
//  - Added a delegate protocol and method to allow the MasterVC to select tableView rows on iPad when moving through
//    the data source with the provided forward and back buttons
//  - TODO: hide back button when we are at the beginning of the list and hide forward button when we are at the end

import UIKit

// Used for forward and back types
enum Direction {
    case forward
    case back
}

protocol DetailViewControllerDelegate : class {
    func didShow(_ detailItem: MasterCellViewModel, at indexPath: IndexPath)
}

class DetailViewController: UIViewController {
    weak var delegate: DetailViewControllerDelegate?

    private lazy var detailView = { DetailView() }()
    var dataSource: CoinDataSource?
    var selectedIndexPath: IndexPath?
    var detailItem: MasterCellViewModel?

    init(with dataSource: CoinDataSource?, selectedIndexPath: IndexPath) {
        self.selectedIndexPath = selectedIndexPath
        super.init(nibName: nil, bundle: nil)
        detailItem = dataSource?[selectedIndexPath.section]?[selectedIndexPath.row]
        self.dataSource = dataSource
        if detailItem != nil {
            self.configureView()
        }
    }

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = detailView
        detailView.forwardBtn.addTarget(self, action: #selector(didTapButton(_:)), for: .touchUpInside)
        detailView.backBtn.addTarget(self, action: #selector(didTapButton(_:)), for: .touchUpInside)
    }

    func configureView() {
        detailView.detailDescriptionLabel.text = "\(detailItem?.name ?? "") - \(detailItem?.priceString ?? "")"
        detailView.buttons(show: true)
    }

    // Since favoriting a coin can also update the data source and sorting, we need to provide
    // a way to keep it up to date in order to preserve the ordering of the forward and back buttons
    func update(dataSource: CoinDataSource?) {
        if  let selectedIndexPath = selectedIndexPath,
            let updatedDataSource = dataSource,
            let currentDataSource = self.dataSource,
            let currentItem = currentDataSource[selectedIndexPath.section]?[selectedIndexPath.row]
        {
            for section in updatedDataSource.keys {
                for (index, item) in (updatedDataSource[section] ?? []).enumerated() {
                    if item.name == currentItem.name {
                        self.selectedIndexPath = IndexPath(item: index, section: section)
                        self.dataSource = updatedDataSource
                        return
                    }
                }
            }
        }

        self.dataSource = dataSource
    }

    // This was a little tricky to implement since we have two tableView sections to traverse.  However, since
    // I had already committed to using two sections, I was commited to provide a seamless solution.

    func increment(indexPath: IndexPath?, direction: Direction) -> IndexPath? {
        guard let indexPath = indexPath, let dataSource = dataSource else { return nil }
        var favRange = 0..<0
        var coinRange = 0..<0

        if let count = dataSource[Section.favorites.rawValue]?.count, count > 0 {
            favRange = 0..<count
        }
        if let count = dataSource[Section.coins.rawValue]?.count, count > 0 {
            coinRange = 0..<count
        }

        var curSection = indexPath.section
        var curRow = indexPath.row

        switch direction {
        case .forward:
            if curSection == Section.favorites.rawValue {
                if curRow == favRange.upperBound-1 {
                    if coinRange.lowerBound == coinRange.upperBound { return nil }
                    curSection = Section.coins.rawValue
                    curRow = 0
                    return IndexPath(item: curRow, section: curSection)
                }
                else {
                    curRow += 1
                    return IndexPath(item: curRow, section: curSection)
                }
            }
            else {
                if curRow == coinRange.upperBound-1 {
                    return nil
                }
                else {
                    curRow += 1
                    return IndexPath(item: curRow, section: curSection)
                }
            }
        case .back:
            if curSection == Section.favorites.rawValue {
                if curRow == 0 {
                    return nil
                }
                else {
                    curRow -= 1
                    return IndexPath(item: curRow, section: curSection)
                }
            }
            else {
                if curRow == 0 {
                    if favRange.upperBound == favRange.lowerBound { return nil }
                    curSection = Section.favorites.rawValue
                    curRow = favRange.upperBound-1
                    return IndexPath(item: curRow, section: curSection)
                }
                else {
                    curRow -= 1
                    return IndexPath(item: curRow, section: curSection)
                }
            }
        }
    }

    // Forward and back button action
    @objc func didTapButton(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            let indexPath = increment(indexPath: selectedIndexPath, direction: .back)
            move(to: indexPath)
            selectedIndexPath = indexPath ?? selectedIndexPath
        case 1:
            let indexPath = increment(indexPath: selectedIndexPath, direction: .forward)
            move(to: indexPath)
            selectedIndexPath = indexPath ?? selectedIndexPath
        default: break
        }
    }

    // Show the coin detail for the coin at indexPath within the tableView DataSource injecting into this class
    func move(to indexPath: IndexPath?) {
        guard let indexPath = indexPath else { return }
        if let detailItem = dataSource?[indexPath.section]?[indexPath.row] {
            self.detailItem = detailItem
            configureView()
            delegate?.didShow(detailItem, at: indexPath)
        }
    }

}

private class DetailView: UIView {

    lazy var backBtn: UIButton = {
        let button = UIButton()
        addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        button.widthAnchor.constraint(equalToConstant: 40.0).isActive = true
        button.setTitle("<=", for: .normal)
        button.isHidden = true
        return button
    }()
    lazy var forwardBtn: UIButton = {
        let button = UIButton()
        addSubview(button)
        button.tag = 1
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        button.widthAnchor.constraint(equalToConstant: 40.0).isActive = true
        button.setTitle("=>", for: .normal)
        button.isHidden = true
        return button
    }()
    lazy var detailDescriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.lineBreakMode = .byTruncatingTail
        self.addSubview(label)
        return label
    }()

    func setupViews() {
        detailDescriptionLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        detailDescriptionLabel.leftAnchor.constraint(equalTo: backBtn.rightAnchor, constant: 10).isActive = true
        detailDescriptionLabel.rightAnchor.constraint(equalTo: forwardBtn.leftAnchor, constant: 10).isActive = true
        backBtn.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        backBtn.centerYAnchor.constraint(equalTo: detailDescriptionLabel.centerYAnchor).isActive = true
        forwardBtn.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        forwardBtn.centerYAnchor.constraint(equalTo: detailDescriptionLabel.centerYAnchor).isActive = true
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        self.backgroundColor = .lightGray
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func buttons(show: Bool) {
        self.forwardBtn.isHidden = !show
        self.backBtn.isHidden = !show
    }

}

